// package people reads from the people.csv and returns a
// sorted any data structure that contains email domains with the number
// of people that has use emails on that domains.
// Errors needs to be logged or handled.
// This is only 3k lines, but could be a large data set as well.
package people

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"os"
	"sort"
	"strings"
)

// Person structure of the row in the file
type Person struct {
	Id        string `json:"Id"`
	FirstName string `json:"FirstName"`
	LastName  string `json:"LastName"`
	Email     string `json:"Email"`
	Gender    string `json:"Gender"`
	IpAddress string `json:"IpAddress"`
}

type People struct {
	Person []Person `json:"People"`
}

type Domain struct {
	Domain string `json:"Domain"`
	Count  int    `json:"Count"`
}

type Domains struct {
	Domains []Domain `json:"Domains"`
}

// GetFileName returns the file name.
func GetFileName() string {
	return "C:\\people.csv"
}

// ReadFile reads the csv file and returns
// a structure within the data.
func ReadFile() People {

	var people People
	file := GetFileName()
	person := Person{}

	csvFile, el := os.Open(file)
	if el != nil {
		fmt.Println(el)
	}

	reader, err2 := csv.NewReader(bufio.NewReader(csvFile)).ReadAll()
	if err2 != nil {
		panic(err2)
	}

	for key, field := range reader {
		if key > 0 {
			person.Id = field[0]
			person.FirstName = field[1]
			person.LastName = field[2]
			person.Email = field[3]
			person.Gender = field[4]
			person.IpAddress = field[5]
			people.Person = append(people.Person, person)
		}
	}
	return people
}

// GetDataByDomain returns all people who are suscribed to an email domain
func GetDataByDomain(people People, domain string) People {

	//Variables
	var data People
	var position int
	var dom string
	for _, person := range people.Person {
		position = strings.Index(person.Email, "@") //Get the position of the @
		dom = person.Email[position+1:]             //Get the email domain

		if strings.ToLower(dom) == domain {
			data.Person = append(data.Person, Person{
				Email:     person.Email,
				FirstName: person.FirstName,
				Gender:    person.Gender,
				Id:        person.Id,
				LastName:  person.LastName,
			})
		} else {
			continue
		}
	}

	if len(data.Person) == 0 {
		fmt.Println("There are not people within that domain")
		return People{}
	}

	return data
}

// GetDataByEmail returns the data for one person by he/she email
func GetDataByEmail(people People, email string) Person {

	for _, person := range people.Person {
		if strings.ToLower(person.Email) == strings.ToLower(email) {
			return person
		}
	}
	return Person{}
}

// SortDescDataStruct this function sort the data as DESC based in the domain name
func SortDescDataStruct(data Domains) Domains {

	if data.Domains != nil {
		sort.Slice(data.Domains, func(i, j int) bool {
			return data.Domains[i].Domain > data.Domains[j].Domain
		})
	} else {
		return Domains{}
	}
	return data
}

// SortAscDataStruct this function sort the data as ASC based in the domain name
func SortAscDataStruct(data Domains) Domains {

	if data.Domains != nil {
		sort.Slice(data.Domains, func(i, j int) bool {
			return data.Domains[i].Domain < data.Domains[j].Domain
		})
	} else {
		return Domains{}
	}
	return data
}

// GetEmailsDomains returns a structure with the required data
func GetEmailsDomains(people People) Domains {

	var position, l, x int
	var dom string
	var data Domains

	for key, person := range people.Person {
		position = strings.Index(person.Email, "@") //Get the position of the @
		dom = person.Email[position+1:]             //Get the email domain
		if key == 1 {
			data.Domains = append(data.Domains, Domain{
				Domain: dom,
				Count:  1,
			})
		} else {
			//Get the len of the current array
			l = len(data.Domains)
			x = 0
			for i, domain := range data.Domains {
				x++ //To compare with length of the elements in the structure
				if domain.Domain == dom {
					//Add 1 to the counter
					data.Domains[i].Count = data.Domains[i].Count + 1
					//Exit the cicle
					break
				} else if x == l && domain.Domain != dom {
					//I'm at the last item of the structure
					data.Domains = append(data.Domains, Domain{
						Domain: dom,
						Count:  1,
					})
					break
				} else {
					continue
				}
			}
		}
	}
	return data
}

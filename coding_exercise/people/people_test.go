package people

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestReadFile(t *testing.T) {

	people := ReadFile()
	if people.Person == nil {
		t.Errorf("Error reading the file")
	} else {
		t.Log("Everything ok")
	}

}

func TestGetEmailsDomains(t *testing.T) {

	people := ReadFile()
	data := GetEmailsDomains(people)

	if data.Domains == nil {
		t.Errorf("Empty structure")
	} else {
		json, err := json.Marshal(data)
		if err != nil {
			t.Errorf("Error when marshal the object")
		}
		fmt.Println(string(json))
		t.Log("Everything ok")
	}
}

func TestSortDescDataStruct(t *testing.T) {
	people := ReadFile()
	domains := GetEmailsDomains(people)
	sortData := SortDescDataStruct(domains)

	if sortData.Domains == nil {
		t.Errorf("Error sorting the data")
	} else {
		json, err := json.Marshal(sortData)
		if err != nil {
			t.Errorf("Error when marshal the object")
		}
		fmt.Println(string(json))
		t.Log("Everything ok")
	}
}

func TestGetDataByDomain(t *testing.T) {
	people := ReadFile()
	people = GetDataByDomain(people, "icq.com")
	if people.Person == nil {
		t.Error("There are no people with that email domain")
	} else {
		json, err := json.Marshal(people)
		if err != nil {
			t.Errorf("Error when marshal the object")
		}
		fmt.Println(string(json))
		t.Log("Everything Ok")
	}
}
func TestSortAscDataStruct(t *testing.T) {
	people := ReadFile()
	domains := GetEmailsDomains(people)
	sortData := SortAscDataStruct(domains)

	if sortData.Domains == nil {
		t.Errorf("Error sorting the data")
	} else {
		json, err := json.Marshal(sortData)
		if err != nil {
			t.Errorf("Error when marshal the object")
		}
		fmt.Println(string(json))
		t.Log("Everything ok")
	}
}

func TestGetDataByEmail(t *testing.T) {
	people := ReadFile()
	person := GetDataByEmail(people, "jlewisham1g@canalblog.com")

	if person.FirstName != "" {
		fmt.Println(person.FirstName)
		t.Logf("Everything ok")
	} else {
		t.Logf("The client does not exist")
	}
}

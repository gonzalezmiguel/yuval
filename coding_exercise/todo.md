### Instructions:
1. Please complete the following as soon as possible (time to market is important to us)
2. Please use a bitbucket *private repo* for your project (private bitbucket repos are free) - do not send me files by email or other channels
3. Errors needs to be logged or handled
4. This is only 3k lines sample, but should work as well with a much larger dataset
5. Create a readme.md with deployment instructions, assumptions and any other information
6. If you have questions like "should the data be sorted asc or desc?", write down your assumption in the readme.md and continue - explain the pro/cons of each assumption taken
7. When you are done with the entire project and the repo is ready, send me a message so we can review your work
8. Unit tests are highly recommended
9. Do not assume that the data in clean - make sure your code is written in a defensive manner
10. We care about speed, good design and clean code

#### Requirements

###### package people
 needs to read from the people.csv and returns a
sorted (any) data structure that contains email domains with the number
of people that has use emails on that domains.
###### Design and Build a REST API (go) using the work from Phase 1 that will allow the following:
1. search for rows by email
2. search for rows by domain
3. build a simple test or client that will utilize the API



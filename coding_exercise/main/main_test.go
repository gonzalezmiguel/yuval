package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestGetAllDomains(t *testing.T) {

	// Create a request to pass to our handler.
	req, err := http.NewRequest("GET", "/api/domains", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder
	resp := httptest.NewRecorder()
	handler := http.HandlerFunc(GetAllDomains)

	handler.ServeHTTP(resp, req)

	if resp.Body.String() == "" {
		t.Error("handler returned unexpected body") //got %v want %v") //,
		//resp.Body.String(), expected)
	} else {
		fmt.Println(resp.Body.String())
		t.Logf("Everything ok")
	}
}

func TestGetDataByDomain(t *testing.T) {

	server := httptest.NewServer(http.HandlerFunc(GetDataByDomain))

	defer server.Close()
	fmt.Println(server.URL)

	url := server.URL

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return
	}
	t.Logf("Everything ok %v", resp.Body)
}

func TestGetDataByEmail(t *testing.T) {

	// Create a request to pass to our handler.
	req, err := http.NewRequest("GET", "/api/domains", nil)
	if err != nil {
		t.Fatal(err)
	}

	// We create a ResponseRecorder
	resp := httptest.NewRecorder()
	handler := http.HandlerFunc(GetAllDomains)

	handler.ServeHTTP(resp, req)

	if resp.Body.String() == "" {
		t.Error("handler returned unexpected body") //got %v want %v") //,
		//resp.Body.String(), expected)
	} else {
		fmt.Println(resp.Body.String())
		t.Logf("Everything ok")
	}
}

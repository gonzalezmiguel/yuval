package main

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	peop "../people"

	"github.com/gorilla/mux"
)

// InvalidParam constant
const InvalidParam = "Invalid parameter"

// InvalidValueParam constant
const InvalidValueParam = "Invalid value param"

// Ok constant
const Ok = "Ok"

// Response basic response structure for an API call
type Response struct {
	Code    string      `json:"Code"`
	Message string      `json:"Message"`
	Body    interface{} `json:"Body"`
}

var people peop.People

// SetResponseBody returns the response body for an API call
func SetResponseBody(code string, message string, body interface{}) Response {

	return Response{
		Code:    code,
		Message: message,
		Body:    body,
	}
}

// GetPeopleDataByDomain returns people data suscribed to a domain
func GetPeopleDataByDomain(people peop.People, domain string) peop.People {
	return peop.GetDataByDomain(people, domain)
}

// GetEmailsDomainsDesc returns all data sorted DESC
func GetEmailsDomainsDesc(people peop.People) peop.Domains {
	domains := peop.GetEmailsDomains(people)
	return peop.SortDescDataStruct(domains)
}

// GetEmailsDomainsAsc returns all data sorted ASC
func GetEmailsDomainsAsc(people peop.People) peop.Domains {
	domains := peop.GetEmailsDomains(people)
	return peop.SortAscDataStruct(domains)
}

// GetUnsortedDomains returns all data without any order
func GetUnsortedDomains(people peop.People) peop.Domains {
	return peop.GetEmailsDomains(people)
}

// GetAllDomains returns all domains with the order specified at SORT parameter
func GetAllDomains(w http.ResponseWriter, r *http.Request) {

	var responseBody Response

	params := r.URL.Query()["sort"]
	var domains peop.Domains

	if len(params) == 0 {
		domains = GetUnsortedDomains(people)
		responseBody = SetResponseBody("0000", Ok, domains)
	} else if len(params) == 1 {
		switch strings.ToLower(params[0]) {
		case "asc":
			domains = GetEmailsDomainsAsc(people)
			responseBody = SetResponseBody("0000", Ok, domains)
		case "desc":
			domains = GetEmailsDomainsDesc(people)
			responseBody = SetResponseBody("0000", Ok, domains)
		default:
			responseBody = SetResponseBody("1000", InvalidValueParam, "")
		}
	} else {
		responseBody = SetResponseBody("1000", InvalidParam, "")
	}

	w.Header().Set("Content-Type", "application/json")
	body, err := json.Marshal(responseBody)
	if err != nil {
		panic(err)
	}
	w.Write(body)
}

// GetDataByDomain returns all the data for people suscribed to an email domain
func GetDataByDomain(w http.ResponseWriter, r *http.Request) {

	var responseBody Response
	var data peop.People

	params := r.URL.Query()
	param := r.URL.Query()["value"]

	if len(params) == 0 {
		responseBody = SetResponseBody("1000", "Parameter DOMAIN required", "")
	} else if len(params) > 1 {
		responseBody = SetResponseBody("1000", InvalidParam, "")
	} else {
		data = GetPeopleDataByDomain(people, strings.ToLower(param[0]))
		if data.Person == nil {
			responseBody = SetResponseBody("2000", "There are not people within the specified domain", "")
		} else {
			responseBody = SetResponseBody("0000", Ok, data)
		}
	}
	w.Header().Set("Content-Type", "application/json")
	body, err := json.Marshal(responseBody)
	if err != nil {
		panic(err)
	}

	w.Write(body)

}

// GetDataByEmail returns all data for one person
func GetDataByEmail(w http.ResponseWriter, r *http.Request) {

	var responseBody Response
	params := r.URL.Query()["value"]

	if len(params) == 0 {
		responseBody = SetResponseBody("1000", "Parameter EMAIL required", "")
	} else if len(params) > 1 {
		responseBody = SetResponseBody("1000", InvalidParam, "")
	} else {
		person := peop.GetDataByEmail(people, strings.ToLower(params[0])) // Get data from the person
		if person.FirstName == "" {
			responseBody = SetResponseBody("0000", "Client does not exit", "")
		} else {
			responseBody = SetResponseBody("0000", Ok, person)
		}
	}

	w.Header().Set("Content-Type", "application/json")
	body, err := json.Marshal(responseBody)
	if err != nil {
		panic(err)
	}
	w.Write(body)

}

func init() {
	people = peop.ReadFile()
}

func main() {

	r := mux.NewRouter().StrictSlash(false) // Difference between equal paths

	r.HandleFunc("/api/domains", GetAllDomains).Methods("GET")
	r.HandleFunc("/api/domains/domain", GetDataByDomain).Methods("GET")
	r.HandleFunc("/api/domains/email", GetDataByEmail).Methods("GET")

	server := &http.Server{
		Addr:           ":60601",
		Handler:        r,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	server.ListenAndServe()
}

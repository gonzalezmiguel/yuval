# yuval
This repository contains information about an API who returns data from a file called people.csv
At the moment to run the code move the folder people.csv to the directory C:\\people.csv or modify the path in the file people.go
in the function GetFileName().

The API runs in the port : 60601 with tree differences GET Methods

/api/domains => parameter { "sort": "asc" }
If this method is called without SORT parameter, it returns the summatory of all people subscribed by email domain without any order.
If this method is called within SORT parameter, it returns the summatory of all people subscribed by email domain with the specified order. (USE asc or desc)


/api/domains/domains => parameter { "value" : "youtube.com" }
This method returns data from the people suscribed to an email domain. Pass the value of the parameter without "@"

/api/domains/email => parameter { "value": "miguel@hotmail.com" }
This method returns data from one person.

To run the API just use the command go run main.go
To test the API use postman to see the result.




